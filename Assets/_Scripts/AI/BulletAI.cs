﻿using UnityEngine;
using System.Collections;

public class BulletAI : MonoBehaviour {

    public Transform target;
    public float speed;
    public EnemyInfo EI;
    public int HitDistance;
    public int dmg;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if(target!=null) transform.LookAt(target);
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        if (target == null)
        {
            Destroy(gameObject);
        }
        else
        {
            if (Vector3.Distance(transform.position, target.position) <= HitDistance)
            {
                target.GetComponent<EnemyInfo>().HP -= dmg;
                Destroy(gameObject);
            }
        }
	}
}
