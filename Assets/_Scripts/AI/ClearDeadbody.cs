﻿using UnityEngine;
using System.Collections;

public class ClearDeadbody : MonoBehaviour {

    public float clearTime;

	// Use this for initialization
	void Start () {
        StartCoroutine("Clear");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator Clear()
    {
       yield return new WaitForSeconds(clearTime);
       Destroy(gameObject);
    }
}
