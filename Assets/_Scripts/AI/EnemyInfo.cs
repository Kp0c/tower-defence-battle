﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyInfo : MonoBehaviour
{

    public int HP = 100;
    public int dmgToPlayer = 1;
    public int price = 5;
    public int ID = 0;
    public int comfortY = 1;
    public GameObject DeadGO;
    public UISlider HPBarSlider;
    public UISprite ForeGround;

    internal int ForPlayerWithId = 0;
    int MaxHP;

    // Use this for initialization
    void Start()
    {
        MaxHP = HP;
        transform.position = new Vector3(transform.position.x, comfortY, transform.position.z);
    }

    public void DamageToPlayer()
    {
        if (ConnectManager.Instance.playerID == ForPlayerWithId)
        {
            ConnectManager.Instance.connection.Send("reduceHP", dmgToPlayer);
        }
        Destroy(HPBarSlider.gameObject.transform.parent.gameObject);
        Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if(HPBarSlider) HPBarSlider.value = (float)HP / MaxHP;
        if(ForeGround) ForeGround.color = new Color((float)1 - (HP / MaxHP), (float)HP / MaxHP, 0);
        if (HP <= 0)
        {
            if(transform.position.x<=GlobalVars.mapHalfWidth)
                ConnectManager.Instance.connection.Send("KillCreep", ID);
            if (DeadGO)
                Instantiate(DeadGO, transform.position, transform.rotation);
            Destroy(HPBarSlider.gameObject.transform.parent.gameObject);
            Destroy(this.gameObject);
        }
    }
}
