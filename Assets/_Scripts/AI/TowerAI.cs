﻿using UnityEngine;
using System.Collections;
using Pathfinding;
using System.Collections.Generic;

public class TowerAI : MonoBehaviour
{

    public float range;
    public GameObject head;
    public GameObject bullet;
    public float rotationSpeed;
    public float reloadCooldown;
    public int TowerDamage = 50;
    public int Price = 100;

    float reloadTimer;
    GameObject target;
    float minDistance = 9999;
    GameObject temp;
    BulletAI bulletTemp;

    // Use this for initialization
    void Start()
    {
        GetComponent<GraphUpdateScene>().Apply();
        reloadTimer = 0;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(0, 1, 1, .3f);
        Gizmos.DrawSphere(transform.position, range);
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            minDistance = 9999;
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

            foreach (GameObject enemy in enemies)
            {
                float dist = Vector3.Distance(this.transform.position, enemy.transform.position);
                if (dist <= range && dist < minDistance)
                {
                    target = enemy;
                    minDistance = dist;
                }
            }
        }
        else
        {
            float dist = Vector3.Distance(this.transform.position, target.transform.position);
            if (dist <= range)
            {
                if (head)
                {
                    head.transform.rotation = Quaternion.Slerp(head.transform.rotation,
                        Quaternion.LookRotation(target.transform.position - head.transform.position),
                        rotationSpeed * Time.deltaTime);
                }
                if (reloadTimer > 0) reloadTimer -= Time.deltaTime;
                if (reloadTimer <= 0)
                {
                    if (bullet)
                    {
                        if (head)
                            temp = (GameObject)Instantiate(bullet, head.transform.position, head.transform.rotation);
                        else
                        {
                            temp = (GameObject)Instantiate(bullet, transform.position, transform.rotation);
                        }
                        bulletTemp = temp.GetComponent<BulletAI>();
                        bulletTemp.dmg = TowerDamage;
                        bulletTemp.target = target.transform;
                    }
                    else
                    {
                        target.GetComponent<EnemyInfo>().HP -= TowerDamage;

                    }
                    reloadTimer = reloadCooldown;
                }
            }
            else target = null;
        }
    }
}
