﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using PlayerIOClient;

public class BuildTower : MonoBehaviour {

    public GameObject InvisibleTower;
    public GameObject currentPlace;
    public Transform startForId1;
    public Transform finishForId1;
    public Transform startForId2;
    public Transform finishForId2;
    //public Transform enemyStart;
    //public Transform enemyFinish;

    private AstarPath AStar;
    private Seeker seeker;
    private GameObject go;
    private GameObject greenTower;

    private bool canPlace = true;
    private bool isShift = false;


	// Use this for initialization
	void Start () {
        AStar = GameObject.Find("A*").GetComponent<AstarPath>();
        seeker = GetComponent<Seeker>();
	}

    void PrepareTower(GameObject tower)
    {
        tower.GetComponent<GraphUpdateScene>().enabled = false;
        tower.GetComponent<TowerAI>().enabled = false;
        tower.GetComponent<Collider>().enabled = false;

        if (!tower.transform.FindChild("AttackRange"))
        {
            GameObject temp = NGUITools.AddChild(tower, GameObject.CreatePrimitive(PrimitiveType.Sphere));
            float attackRange = Mathf.Sqrt(tower.GetComponent<TowerAI>().range);
            temp.name = "AttackRange";
            temp.transform.localPosition = new Vector3(0, 0, 0);
            temp.transform.localScale = new Vector3(attackRange, 0, attackRange);
        }

        MeshRenderer[] allMeshes = tower.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < allMeshes.Length; i++)
        {
            for (int j = 0; j < allMeshes[i].renderer.materials.Length; j++)
            {
                allMeshes[i].renderer.materials[j].shader = Shader.Find("Transparent/Diffuse");
                allMeshes[i].renderer.materials[j].color = new Color(0f, 1f, 0f, .5f);
                if (allMeshes[i].GetComponent<Collider>())
                    allMeshes[i].GetComponent<Collider>().enabled = false;
            }
        }
        if (tower.renderer)
            for (int j = 0; j < tower.renderer.materials.Length; j++)
            {
                tower.renderer.materials[j].color = new Color(0f, 1f, 0f, .3f);
            }
    }

    public void CantPlace()
    {
        canPlace = false;
    }

    public void AllowPlace()
    {
        canPlace = true;
    }
	
	// Update is called once per frame
    void Update()
    {
        if (GlobalVars.selectedTowerIndex >= 0)
        {
            //Input
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                isShift = true;
            }
            else
            {
                isShift = false;
            }

            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                GlobalVars.selectedTowerIndex = -1;
            }
            //End

            if (currentPlace != null)
            {
                if (greenTower == null)
                {
                    greenTower = Instantiate(GlobalVars.GetTowerFromIndex(),
                     currentPlace.transform.position, currentPlace.transform.rotation) as GameObject;
                    PrepareTower(greenTower);
                }
                else
                {
                    if (greenTower.name != GlobalVars.GetCurrentTowerName()+"(Clone)")
                    {
                        Destroy(greenTower);
                    }
                }
                if(greenTower) greenTower.transform.position = currentPlace.transform.position;
            }
        }
        else
        {
            DestroyImmediate(greenTower);
        }

        if (Input.GetKeyUp(KeyCode.Mouse0) && canPlace && currentPlace != null && AStar != null
            && GlobalVars.selectedTowerIndex >= 0)
        {
            if (ConnectManager.Instance.Money >= GlobalVars.GetCurrentPrice())
            {
                ConnectManager.Instance.Money -= GlobalVars.GetCurrentPrice();
                go = Instantiate(InvisibleTower, currentPlace.transform.position, currentPlace.transform.rotation)
                as GameObject;
                AStar.Scan();                    
                switch (ConnectManager.Instance.playerID)
                {
                    case 1:
                        seeker.StartPath(startForId1.position, finishForId1.position, OnPathComplete);
                        break;
                    case 2:
                        seeker.StartPath(startForId2.position, finishForId2.position, OnPathComplete);
                        break;
                }
            }
            else
            {
                ConnectManager.Instance.errors.Add("У вас не хватит денег на " + GlobalVars.GetCurrentTowerName());
                GlobalVars.selectedTowerIndex = -1;
            }
        }

        if (ConnectManager.Instance.addtowers.Count > 0)
        {
            foreach (Message m in ConnectManager.Instance.addtowers)
            {
                    Instantiate(GlobalVars.GetTowerFromIndex(m.GetInt(0)),
                        new Vector3(m.GetInt(1), 1, m.GetInt(2)), new Quaternion());
            }
            ConnectManager.Instance.addtowers.Clear();
        }
    }

    void OnPathComplete(Path p)
    {
        if (p.error)
        {
            ConnectManager.Instance.errors.Add("Нельзя перекрывать путь");
        }
        else
        {
            ConnectManager.Instance.connection.Send("addTower", GlobalVars.selectedTowerIndex, go.transform.position.x,
                     go.transform.position.z);
            
        }
        if (!isShift)
        {
            GlobalVars.selectedTowerIndex = -1;
        }
        DestroyImmediate(go);
    }
}
