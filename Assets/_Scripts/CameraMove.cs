﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

    private Transform me;

    public int scrollSpeed = 15;
    public int minHeight = 1;
    public int maxHeight = 60;
    public int borderLeft = 40;
    public int borderRight = 185;
    public int borderUp = 70;
    public int borderDown = 30;

	// Use this for initialization
	void Start () {
        me = transform;
	}
	
	// Update is called once per frame
	void Update () {
        if (((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow) || 
            Input.mousePosition.y>=Screen.height-5) && me.position.z <= borderUp))
        {
            me.position = me.position + Vector3.forward;
        }
        if (((Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow) || 
            Input.mousePosition.y<=5) && me.position.z >= borderDown))
        {
            me.position = me.position + Vector3.back;
        }
        if (((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) || 
            Input.mousePosition.x<=5) && me.position.x >= borderLeft))
        {
            me.position = me.position + Vector3.left;
        }
        if (((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) ||
            Input.mousePosition.x >= Screen.width - 5) && me.position.x <= borderRight))
        {
            me.position = me.position + Vector3.right;
        }
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            me.position = new Vector3(me.position.x, me.position.y - Input.GetAxis("Mouse ScrollWheel") * 
                scrollSpeed, me.position.z);
            if (me.position.y >= maxHeight) me.position = new Vector3(me.position.x, maxHeight, me.position.z);
            if (me.position.y <= minHeight) me.position = new Vector3(me.position.x, minHeight, me.position.z);
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                me.rotation = Quaternion.AngleAxis(-90+(maxHeight-me.position.y), Vector3.left);
            }
        }
	}
}
