﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class AddEnemyAI : EditorWindow
{

    [MenuItem("TDB/EnemyAI")]
    public static void CreateWindow()
    {
        AddEnemyAI window = GetWindow<AddEnemyAI>();
        window.title = "Add enemy AI";
        window.autoRepaintOnSceneChange = true;
    }

    void OnSelectionChange() { Repaint(); }

    public GameObject target;
    public GameObject deadBody;
    public int speed = 15;
    public int turningSpeed = 5;
    public int HP = 17;
    public int DmgToPlayer = 1;
    public int Price = 5;
    public int ID = 0;

    bool showWarning = false;
    bool initialized = false;

    void Init()
    {
        initialized = true;
        speed =         PlayerPrefs.GetInt("speed", 15);
        turningSpeed =  PlayerPrefs.GetInt("turningSpeed", 5);
        HP =            PlayerPrefs.GetInt("HP", 17);
        DmgToPlayer =   PlayerPrefs.GetInt("DmgToPlayer", 1);
        Price =         PlayerPrefs.GetInt("Price", 5);
        ID =            PlayerPrefs.GetInt("ID", 0);
    }

    void Save()
    {
        PlayerPrefs.SetInt("speed", speed);
        PlayerPrefs.SetInt("turningSpeed", turningSpeed);
        PlayerPrefs.SetInt("HP", HP);
        PlayerPrefs.SetInt("DmgToPlayer", DmgToPlayer);
        PlayerPrefs.SetInt("Price", Price);
        PlayerPrefs.SetInt("ID", ID);
    }

    public void OnDestroy()
    {
        Save();
    }

    public void OnGUI()
    {
        if (!initialized) Init();
        if(Selection.activeGameObject!=null) target = Selection.activeGameObject;
        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Target",GUILayout.Width(120));
        target = (GameObject)EditorGUILayout.ObjectField(target, typeof(GameObject), true);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Dead body", GUILayout.Width(120));
        deadBody = (GameObject)EditorGUILayout.ObjectField(deadBody, typeof(GameObject), true);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Speed", GUILayout.Width(120));
        speed = EditorGUILayout.IntField(speed);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Turn speed", GUILayout.Width(120));
        turningSpeed = EditorGUILayout.IntField(turningSpeed);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Hit point", GUILayout.Width(120));
        HP = EditorGUILayout.IntField(HP);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Damage to player", GUILayout.Width(120));
        DmgToPlayer = EditorGUILayout.IntField(DmgToPlayer);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Price", GUILayout.Width(120));
        Price = EditorGUILayout.IntField(Price);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("ID", GUILayout.Width(120));
        ID = EditorGUILayout.IntField(ID);
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Add/Update"))
        {
            if (target == null) ShowNotification(new GUIContent("Select target!"));
            else
            {
                if (GameObject.Find("GameManager"))
                {
                    showWarning = true;
                    target.tag = "Enemy";
                    target.layer = 8;
                    GlobalVars gv = GameObject.Find("GameManager").GetComponent<GlobalVars>();
                    if (gv.enemies.Count >= ID + 1)
                    {
                        gv.enemies[ID] = target;
                    }
                    else gv.enemies.Add(target);

                    //Enemy info
                    EnemyInfo EI = target.AddMissingComponent<EnemyInfo>();
                    EI.HP = HP;
                    EI.dmgToPlayer = DmgToPlayer;
                    EI.price = Price;
                    EI.ID = ID;
                    if(deadBody!=null) EI.DeadGO = deadBody;

                    target.AddMissingComponent<Seeker>();
                    target.AddMissingComponent<CharacterController>();

                    //AI Path
                    AIPath AP = target.AddMissingComponent<AIPath>();
                    AP.speed = speed;
                    AP.turningSpeed = turningSpeed;
                    AP.slowdownDistance = 0;
                    AP.pickNextWaypointDist = 3;
                    AP.endReachedDistance = 0.5f;

                    ShowNotification(new GUIContent("added or updated states"));
                    Save();
                }
                else
                {
                    ShowNotification(new GUIContent("Can't find \"GameManager\""));
                }
            }
        }
        if (showWarning)
        {
            EditorGUILayout.HelpBox("Сейчас надо добавить/обновить и настроить иконку в HUD", MessageType.Warning);
            EditorGUILayout.HelpBox("Сейчас надо обновить инфу на сервере", MessageType.Warning);
            EditorGUILayout.HelpBox("Сейчас надо добавить код в SpawnManager.cs", MessageType.Warning);
        }
        GUILayout.EndVertical();
    }
}
