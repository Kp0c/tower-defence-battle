﻿using UnityEngine;
using System.Collections;

public class ErrorManager : MonoBehaviour
{

    public string ErrorPlaneName;
    AudioSource audioSource;
    public AudioClip errorSound;
    public GameObject errPref;
    GameObject tempgo;

    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (ConnectManager.Instance.errors.Count > 0)
        {
            tempgo = (GameObject)Instantiate(errPref);
            tempgo.transform.parent = GameObject.Find(ErrorPlaneName).transform;
            tempgo.GetComponent<UILabel>().text = ConnectManager.Instance.errors[0];
            ConnectManager.Instance.errors.RemoveAt(0);
            audioSource.PlayOneShot(errorSound);
            Destroy(tempgo, 5f);
        }

    }
}
