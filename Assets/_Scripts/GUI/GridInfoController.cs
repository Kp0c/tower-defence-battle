﻿using UnityEngine;
using System.Collections;

public class GridInfoController : MonoBehaviour {

    GameObject temp;
    int ID;

	// Use this for initialization
	void Start () {
        StartCoroutine(UpdateInfo());
	}
	
	// Update is called once per frame
	void Update () {

	}

    IEnumerator UpdateInfo()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            for (int i = 0; i < transform.childCount; i++)
            {
                temp = transform.GetChild(i).gameObject;
                ID = GlobalVars.GetEnemyIDFromName(temp.name);
                if (ID >= 0)
                {
                    for (int j = 0; j < temp.transform.childCount; j++)
                    {
                        switch (temp.transform.GetChild(j).name)
                        {
                            case "Name":
                                temp.transform.GetChild(j).GetComponent<UILabel>().text = temp.name;
                                break;
                            case "Price":
                                temp.transform.GetChild(j).GetComponent<UILabel>().text =
                                    GlobalVars.GetEnemyPrice(ID).ToString();
                                break;
                            case "Preview":
                                temp.transform.GetChild(j).GetComponent<UISprite>().spriteName =
                                    temp.name + "_preview";
                                //temp.transform.GetChild(j).GetComponent<UISprite>().Update();
                                break;
                        }
                    }
                }
            }
        }
    }
}
