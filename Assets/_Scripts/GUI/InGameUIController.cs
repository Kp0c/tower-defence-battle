﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InGameUIController : MonoBehaviour
{
    public UILabel Money;
    public UILabel Income;
    public UILabel Lives;
    public UILabel EnemyLives;

    public UILabel InfoLabel;

    public UITextList textList;

	void Start () {
        InfoLabel.parent.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (GlobalVars.selectedTowerIndex < 0)
        {
            InfoLabel.parent.gameObject.SetActive(false);
        }
        Money.text = "Золота: " + ConnectManager.Instance.Money;
        Income.text = "Доход: " + ConnectManager.Instance.Income;
        Lives.text = "Жизней: " + ConnectManager.Instance.Lives;
        EnemyLives.text = "Жизней у врага: " + ConnectManager.Instance.EnemyLives;

        if (GlobalVars.selectedTowerIndex >= 0)
        {
            TowerAI temp = GlobalVars.GetCurrentTowerAI();
            InfoLabel.text = "Название: "+temp.name+"\n" +
                                 "Урон: " + temp.TowerDamage + "\n" +
                                 "Цена: " + temp.Price + "\n" +
                                 "Перезарядка: " + temp.reloadCooldown + "\n" +
                                 "Дистанция атаки: " + temp.range;
        }

        if (!string.IsNullOrEmpty(ConnectManager.Instance.chat))
        {
            textList.Add(ConnectManager.Instance.chat);
            ConnectManager.Instance.chat = "";
        }
	}

    public void SetFullScreen()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }

    private void CheckTowerPrice()
    {
        if (ConnectManager.Instance.Money >= GlobalVars.GetCurrentPrice())
        {
            InfoLabel.parent.gameObject.SetActive(true);
        }
        else
        {
            ConnectManager.Instance.errors.Add("У вас не хватает денег на " + GlobalVars.GetCurrentTowerName());
            GlobalVars.selectedTowerIndex = -1;
        }
    }

    public void SelectWizardTower()
    {
        GlobalVars.selectedTowerIndex = 0;
        CheckTowerPrice();
    }

    public void SelectWoodenTower()
    {
        GlobalVars.selectedTowerIndex = 1;
        CheckTowerPrice();
    }
}