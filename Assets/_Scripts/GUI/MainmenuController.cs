﻿using UnityEngine;
using System.Text.RegularExpressions;
using PlayerIOClient;
using System;

public class MainmenuController : MonoBehaviour {

    public GameObject LoginPanel;
    public GameObject CreateJoinPanel;
    public GameObject CreateRoomPanel;
    public GameObject JoinRoomPanel;
    public GameObject RoomsOwner;
    public GameObject Preloader;

    public UIScrollView goScrollView;
    public GameObject RoomPrefab;

    public UIGrid Grid;

    public UILabel JoinPanelRoomName;
    public UILabel JoinPanelCreatorName;

    public UIInput CreateRoomName;
    public UIInput Login;
    public UIInput Password;

    public UILabel UserName;
    public UISlider Exp;

    Regex CorrectRegex;
    GameObject tempGO;
    bool logedIn = false;
    RoomInfo selectedRoom;

    public void Start()
    {
        CorrectRegex = new Regex("[a-zа-я0-9]");
    }

    //TODO: сделать комнату ожидания второго игрока. С чатом и ... :D
    public void Update()
    {
        if (ConnectManager.Instance.player != null)
        {
            UserName.text = ConnectManager.Instance.Username;
            Exp.value = (float)ConnectManager.Instance.Exp / 1000f;
        }
        if (logedIn && ConnectManager.Instance.player!=null)
        {
            Preloader.SetActive(false);
            logedIn = false;
            CreateJoinPanel.SetActive(true);
        }
        if (ConnectManager.Instance.ErrorWhileLoginRegister)
        {
            LoginPanel.SetActive(true);
            logedIn = false;
            Preloader.SetActive(false);
            ConnectManager.Instance.ErrorWhileLoginRegister = false;
        }

        if (JoinRoomPanel.activeSelf)
        {
            foreach (RoomInfo room in ConnectManager.Instance.servers)
            {
                if (!RoomsOwner.transform.FindChild(room.Id))
                {
                    tempGO = (GameObject)Instantiate(RoomPrefab);
                    tempGO.GetComponent<UIButton>().onClick.Add(new EventDelegate(tempGO.GetComponent<UIButton>(),
                        "RoomOnClick"));
                    tempGO.GetComponent<UIButton>().mainMenu = this.gameObject;
                    tempGO.GetComponent<UIDragScrollView>().scrollView = goScrollView;
                    tempGO.name = room.Id;
                    tempGO.transform.parent = RoomsOwner.transform;
                    tempGO.transform.localScale = new Vector3(1, 1, 1);
                    tempGO.GetComponentInChildren<UILabel>().text = room.Id;
                    tempGO.transform.localPosition = new Vector3(0, 0, 0);
                }
            }
            for (int i = 0; i < RoomsOwner.transform.childCount; i++)
            {
                if (!ConnectManager.Instance.servers.Exists(room =>room.Id == RoomsOwner.transform.GetChild(i).name))
                {
                    Destroy(RoomsOwner.transform.GetChild(i).gameObject);
                }
            }
            //Grid.Reposition();
            Grid.repositionNow = true;
            //goScrollView.ResetPosition();
            ConnectManager.Instance.GetRooms();
        }
    }

    public void btnRoomClick(GameObject sender)
    {
        selectedRoom = ConnectManager.Instance.servers.Find(room => room.Id == sender.name);
        JoinPanelRoomName.text = "Имя комнаты: " + selectedRoom.Id;
        JoinPanelCreatorName.text = "Создатель комнаты: " + selectedRoom.RoomData["creator"];
    }

    public void btnJoinToRoom()
    {
        JoinRoomPanel.SetActive(false);
        Preloader.SetActive(true);
        //JOIN TO ROOM
        if (ConnectManager.Instance.servers.Exists(room => room.Id == selectedRoom.Id))
        {
            ConnectManager.Instance.Join(selectedRoom.Id);
            Application.LoadLevel(1);
        }
        else
        {
            ConnectManager.Instance.errors.Add("Этой комнаты уже не существует");
            Preloader.SetActive(false);
            JoinRoomPanel.SetActive(true);
            btnRefreshClick();
        }
        
        
    }

    public void btnLoginClick()
    {
        if (CorrectRegex.IsMatch(Login.value))
        {
            LoginPanel.SetActive(false);
            //ConnectManager.Instance.Connect(Login.value);
            ConnectManager.Instance.Login(Login.value, Password.value);
            logedIn = true;
            Preloader.SetActive(true);
        }
        else
        {
            ConnectManager.Instance.errors.Add("Введите корректный логин (a-z;а-я;0-9)");
        }
    }

    public void btnRegisterClick()
    {
        if (CorrectRegex.IsMatch(Login.value) && CorrectRegex.IsMatch(Password.value))
        {
            LoginPanel.SetActive(false);
            ConnectManager.Instance.Register(Login.value, Password.value);
            logedIn = true;
            Preloader.SetActive(true);
        }
        else
        {
            ConnectManager.Instance.errors.Add("Введите корректный логин (a-z;а-я;0-9)");
        }
    }

    public void btnCreateClick()
    {
        CreateJoinPanel.SetActive(false);
        CreateRoomPanel.SetActive(true);
    }

    public void btnJoinClick()
    {
        CreateJoinPanel.SetActive(false);
        JoinRoomPanel.SetActive(true);
        btnRefreshClick();
    }

    public void btnCreateRoomClick()
    {
        if (CorrectRegex.IsMatch(CreateRoomName.value))
        {
            CreateRoomPanel.SetActive(false);
            Preloader.SetActive(true);
            //CREATE ROOM
            ConnectManager.Instance.Create(CreateRoomName.value);
            Application.LoadLevel(1);
        }
        else
        {
            ConnectManager.Instance.errors.Add("Введите корректное имя комнаты");
        }
    }

    public void btnRefreshClick()
    {
        ConnectManager.Instance.GetRooms();
        for (int i = 0; i < RoomsOwner.transform.childCount; i++)
        {
            Destroy(RoomsOwner.transform.GetChild(i).gameObject);
        }
    }

    public void btnBackClick()
    {
        CreateRoomPanel.SetActive(false);
        JoinRoomPanel.SetActive(false);
        CreateJoinPanel.SetActive(true);
    }
}
