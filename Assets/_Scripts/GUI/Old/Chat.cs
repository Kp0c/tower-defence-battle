﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlayerIOClient;

public class Chat : MonoBehaviour {

    Vector2 chatPosition;
    Vector2 prevPosition;
    bool showChat = false;
    string chat="";
    string msg="";
    List<string> cheats = new List<string>() {"work","towerMe" };

    bool showError = false;
    float time;
    string Error;
    AudioSource audioSource;
    public AudioClip errorSound;
    public GUIStyle guiStyleLabelError;
    
    public float ShowTime;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        DontDestroyOnLoad(this.gameObject);
    }

    void OnGUI()
    {
        GUILayout.BeginArea(new Rect(Screen.width / 2 - 300, Screen.height / 2 + Screen.height / 4, 600, 150));
        if (showChat)
        {
            chatPosition = GUILayout.BeginScrollView(chatPosition, GUILayout.Width(600), GUILayout.Height(150));
            GUILayout.TextArea(ConnectManager.Instance.chat);
            GUILayout.EndScrollView();
        }
        GUILayout.EndArea();

        var returnHit = (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Return);
        GUILayout.BeginArea(new Rect(Screen.width / 2 - 300, Screen.height / 2 + Screen.height / 4 + 150, 600, 30));
        GUI.SetNextControlName("Chat");
        msg = GUILayout.TextField(msg, GUILayout.Width(600), GUILayout.Height(20));
        
        GUILayout.EndArea();
        if (returnHit)
        {
            if (msg == "") GUI.FocusControl("Chat");
            else
            {
                if (GlobalVars.isDEBUG)
                {
                    if (cheats.Contains(msg))
                    {
                        switch (msg)
                        {
                            case "work":
                                ConnectManager.Instance.chat += "Cheats ON";
                                break;
                            case "towerMe":
                                ConnectManager.Instance.addtowers.Add(
                                    Message.Create("addTower","Kp0c",1,95,5));
                                break;
                        }
                        msg = "";
                        GUI.FocusControl("");
                    }
                    else
                    {
                        ConnectManager.Instance.chat += msg;
                        msg = "";
                        GUI.FocusControl("");
                    }
                }
                else
                {
                 
                    ConnectManager.Instance.connection.Send("chat", msg);
                    //ConnectManager.Instance.chat += msg;
                    msg = "";
                    GUI.FocusControl("");
                }
            }
        }

        if (showError)
        {
            GUI.Label(new Rect(Screen.width / 2 - Error.Length * 10, Screen.height / 2 - 50, 400, 100), Error, guiStyleLabelError);
        }
    }	
	
	void Update () {

        //Update errors
        if (showError)
        {
            if (time >= 0 && time < 2)
            {
                time += Time.deltaTime;
                guiStyleLabelError.normal.textColor = new Color(1, 0, 0, 2 - time);
            }
            if (time >= 2)
            {
                time = 0;
                showError = false;
                Error = "";
            }
            while (ConnectManager.Instance.errors.Contains(Error))
            {
                ConnectManager.Instance.errors.Remove(Error);
            }
        }
        else
            if (ConnectManager.Instance.errors.Count > 0)
            {
                showError = true;
                audioSource.PlayOneShot(errorSound);
                Error = ConnectManager.Instance.errors[0];
            }

        //Update chat
        if (chat != ConnectManager.Instance.chat)
        {
            showChat = true;
            chat = ConnectManager.Instance.chat;
            chatPosition = new Vector2(0, 1000000);
        }
        if (ConnectManager.Instance.time <= ShowTime)
        {
            showChat = true;
            ConnectManager.Instance.time += Time.deltaTime;
            GUI.contentColor = new Color(1, 1, 1, (ShowTime-ConnectManager.Instance.time)*51);
        }
        if (ConnectManager.Instance.time > ShowTime)
        {
            showChat = false;
        }
        if (chatPosition != prevPosition)
        {
            ConnectManager.Instance.time = 0;
        }
        prevPosition = chatPosition;
	}
}
