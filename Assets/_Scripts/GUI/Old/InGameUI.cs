﻿using UnityEngine;
using System.Collections;

public class InGameUI : MonoBehaviour
{
    Vector2 scrollPosition;

    public GameObject[] TowerGO;
    public Texture2D[] Towers;
    public GameObject GameManager;

    void OnGUI()
    {
        GUILayout.BeginVertical();
        GUILayout.Box("Money: "+ConnectManager.Instance.Money);
        GUILayout.Box("Income: " + ConnectManager.Instance.Income);
        GUILayout.Box("Lives: " + ConnectManager.Instance.Lives);
        GUILayout.Box("Enemy lives: " + ConnectManager.Instance.EnemyLives);
        GUILayout.EndVertical();

        GUILayout.BeginArea(new Rect(0, Screen.height-151,Screen.width,151));
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(Screen.width),
            GUILayout.Height(150));
        for (int i = 0; i < Towers.Length; i++)
        {
            if (GUILayout.Button(Towers[i], GUILayout.Width(100), GUILayout.Height(140)))
            {
                GlobalVars.selectedTowerIndex = i + 1;
            }
        }
        GUILayout.EndScrollView();
        GUILayout.EndArea();
        if (GlobalVars.selectedTowerIndex-1 >= 0)
        {
            GUILayout.BeginArea(new Rect(Screen.width - 300, 0, 300, Screen.height - 150));
            GUILayout.Box("Selected tower: " + TowerGO[GlobalVars.selectedTowerIndex - 1].GetComponent<TowerAI>().name);
            GUILayout.Box("Damage: " + TowerGO[GlobalVars.selectedTowerIndex - 1].GetComponent<TowerAI>().TowerDamage);
            GUILayout.EndArea();
        }
    }
}
