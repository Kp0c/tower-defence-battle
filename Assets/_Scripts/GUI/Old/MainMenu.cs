﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{

    string Login = "";
    string RoomName = "";
    bool isCreate = false;
    bool isJoin = false;
    Vector2 scrollPosition;

    public GUISkin guiSkin;

    string DeleteTrash(string str)
    {
        str = str.Replace(" ", "");
        str = str.Replace("!", "");
        str = str.Replace("@", "");
        str = str.Replace("#", "");
        str = str.Replace("$", "");
        str = str.Replace("%", "");
        str = str.Replace("^", "");
        str = str.Replace("&", "");
        str = str.Replace("*", "");
        return str;
    }

    void OnGUI()
    {
        GUI.Box(new Rect(Screen.width / 2 - 300, Screen.height / 2 - 200, 600, 400), "Main menu", guiSkin.box);
        #region main menu
        if (ConnectManager.Instance.player == null)
        {
            Login = GUI.TextField(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 100, 100, 25),
                Login, 20, guiSkin.textField);
            GUI.Label(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 125, 150, 25),
                "Enter your nickname", guiSkin.label);
            if (GUI.Button(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 100, 100, 25), "Login", guiSkin.button))
            {
                Login = DeleteTrash(Login);
                if (Login != "")
                {
                    //FIX it if i wamt return to old GUI
                    //ConnectManager.Instance.Connect(Login);
                    print("login with nickname " + Login);
                }
                else
                {
                    ConnectManager.Instance.errors.Add("Введите ник без спец. знаков");
                }

            }
        }
        #endregion
        else
        {
            if(isCreate || isJoin)
                if (GUI.Button(new Rect(Screen.width / 2 - 50, Screen.height / 2 + 100, 100, 25), "Back", guiSkin.button))
                {
                    isCreate = false;
                    isJoin = false;
                }

            #region create new room
            if (isCreate)
            {
                RoomName = GUI.TextField(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 100, 100, 25),
                RoomName, 20, guiSkin.textField);
                GUI.Label(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 125, 150, 25),
                    "Enter room name", guiSkin.label);
                if (GUI.Button(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 100, 100, 25), "Create", guiSkin.button))
                {
                    RoomName = DeleteTrash(RoomName);
                    if (RoomName != "")
                    {
                        ConnectManager.Instance.Create(RoomName);
                        Application.LoadLevel(1);
                    }
                    else
                    {
                        ConnectManager.Instance.errors.Add("Введите имя комнаты без спец. знаков");
                    }
                }
            }
            #endregion
            else
                #region join
                if (isJoin)
                {
                    if (ConnectManager.Instance.servers != null)
                    {
                        GUILayout.BeginArea(new Rect(Screen.width / 2 - 200, Screen.height / 2-150, 400, 225));
                        scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(400),
                            GUILayout.Height(225));
                        for (int i = 0; i < ConnectManager.Instance.servers.Count; i++)
                        {
                            //if (GUILayout.Button(ConnectManager.Instance.servers[i]))
                            //{
                            //    ConnectManager.Instance.Join(ConnectManager.Instance.serversId[i]);
                            //    Application.LoadLevel(1);
                            //}
                        }
                        GUILayout.EndScrollView();
                        GUILayout.EndArea();
                    }
                }
                #endregion
                else
                {

                    if (GUI.Button(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 100, 100, 25), "Create", guiSkin.button))
                    {
                        isCreate = true;
                    }
                    if (GUI.Button(new Rect(Screen.width / 2 - 50, Screen.height / 2, 100, 25), "Join", guiSkin.button))
                    {
                        isJoin = true;
                        ConnectManager.Instance.GetRooms();
                    }
                }
        }
    }
}

