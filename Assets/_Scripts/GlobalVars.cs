﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalVars : MonoBehaviour {

    public static int mapHalfWidth = 130;
    public static bool isDEBUG = false;
    public static int selectedTowerIndex = -1;
    //зависит от selectedToewrIndex, так что чётко следовать индексам!
    public List<GameObject> towers = new List<GameObject>();

    public List<GameObject> enemies = new List<GameObject>();

    static GlobalVars gv;

    public void Start(){
        gv = this;  
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ID">0 - TestCreep</param>
    /// <returns></returns>
    public static int GetEnemyPrice(int ID)
    {
        return gv.enemies.Find(enemy => enemy.GetComponent<EnemyInfo>().ID == ID)
            .GetComponent<EnemyInfo>().price;
    }

    public static int GetCurrentPrice()
    {
        return gv.towers[selectedTowerIndex].GetComponent<TowerAI>().Price;
    }

    public static TowerAI GetCurrentTowerAI()
    {
        return gv.towers[selectedTowerIndex].GetComponent<TowerAI>();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="index">0 - WizardTower</param>
    /// <returns></returns>
    public static GameObject GetTowerFromIndex(int index)
    {
        return gv.towers[index];
    }

    public static GameObject GetTowerFromIndex()
    {
        return gv.towers[selectedTowerIndex];
    }


    /// <param name="index">0 - TestCreep</param>
    public static GameObject GetEnemyFromIndex(int index)
    {
        return gv.enemies[index];
    }

    public static int GetEnemyIDFromName(string name)
    {
        return gv.enemies.FindIndex(enemy => enemy.name == name);
    }

    public static string GetCurrentTowerName()
    {
        if (selectedTowerIndex >= 0) return gv.towers[selectedTowerIndex].GetComponent<TowerAI>().name;
        else return "Error: index = " + selectedTowerIndex;
    }
}
