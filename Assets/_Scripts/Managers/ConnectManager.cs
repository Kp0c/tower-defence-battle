﻿using UnityEngine;
using System.Collections;
using PlayerIOClient;
using System.Collections.Generic;
using System;

public class ConnectManager {

    public const string GAME_ID = "towerdefence-0409ph0reelwqkhmshyrw";
    public const string GameType = "TDB main roomType";
    public Client player;
    public int playerID = -1;
    public string roomName = "";
    public List<string> errors = new List<string>();
    public Connection connection;
    public int Looser = 0;  //0 - not looser, not winner; 1 - winner; 2 - looser;
    /*....................................................*/
    public float time = 0;
    public string chat = "";
    /*.............MAIN MENU CONTROLLER....................*/
    public bool ErrorWhileLoginRegister = false;
    public string Username = "username";
    public string Password = "password";
    public int Exp = 0;
    public List<RoomInfo> servers = new List<RoomInfo>();
    /*.................IN-GAME.............................*/
    public List<Message> addtowers = new List<Message>();
    public List<Message> addenemies = new List<Message>();
    public int Money=0;
    public int Lives = 0;
    public int Income = 0;
    public int EnemyLives = 0;

    private static readonly ConnectManager instance = new ConnectManager();

	public void Login(string username, string password) {
        this.Username = username;
        this.Password = password;
        //PlayerIO.Connect(GAME_ID, "admin", Nickname, null, null, null, connected,error);
        PlayerIO.Authenticate(GAME_ID, "public", new Dictionary<string, string> {                       
        {"username", username},         
        {"password", password},        
                             },
        null,
        connected,
        delegate(PlayerIOError err)
        {
            switch (err.ErrorCode)
            {
                case ErrorCode.InvalidAuth:
                    Debug.Log("InvalidAuth, try repeat Login");
                    Login(username, password);
                    break;
                default:
                    error(err);
                    break;
            }
        });
	}

    public void Register(string username, string password)
    {
        this.Username = username;
        this.Password = password;
        PlayerIO.Authenticate(GAME_ID, "public", new Dictionary<string, string> {        
        {"register", "true"},               
        {"username", username},         
        {"password", password},        
                             },
        null,
        connected,
        delegate(PlayerIOError err){
            switch (err.ErrorCode)
            {
                case ErrorCode.InvalidAuth:
                    Debug.Log("InvalidAuth, try repeat register");
                    Register(username, password);
                    break;
                default:
                    error(err);
                    break;
            }
        });
    }

    public void Create(string RoomName)
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        dict.Add("creator", player.ConnectUserId);
        player.Multiplayer.CreateRoom(RoomName, GameType, true, dict,created,error);
    }

    public void GetRooms()
    {
        player.Multiplayer.ListRooms(GameType, null, 100, 0, successGetRooms, error);
    }

    public void Join(string RoomName)
    {
        Looser = 0;
        player.Multiplayer.JoinRoom(RoomName, null, joined, error);
    }

    private void successGetRooms(RoomInfo[] rooms)
    {
        servers.Clear();
        for(int i=0;i<rooms.Length;i++)
        {
            if (rooms[i].OnlineUsers < 2)
            {
                servers.Add(rooms[i]);
            }
        }
    }

    public static ConnectManager Instance
    {
        get { return instance; }
    }

    private void connected(Client client)
    {
        player = client;
        //GET STATE OF USER FROM DB
        client.BigDB.LoadOrCreate("Users", client.ConnectUserId, delegate(DatabaseObject result)
        {
            if (!result.Contains("username"))
            {
                result.Set("username", Username);
            }
            else Username = result.GetValue("username") as string;

            if (!result.Contains("exp"))
            {
                result.Set("exp", 0);
            }
            else Exp =  Convert.ToInt32(result.GetValue("exp"));
            result.Save();
        },error);
    }

    private void created(string roomName)
    {
        this.roomName = roomName;
        Join(roomName);
    }

    private void joined(Connection conn)
    {
        this.connection = conn;
        connection.OnMessage += parseMessage;
    }

    private void parseMessage(object sender,Message m)
    {
        switch (m.Type)
        {
            case "chat":
                chat += m.GetString(0) + "\n";
                time = 0;
                break;
            case "UpdateState":
                Lives=m.GetInt(0);
                Money=m.GetInt(1);
                Income=m.GetInt(2);
                EnemyLives=m.GetInt(3);
                break;
            case "addTower":
                addtowers.Add(m);
                break;
            case "sendEnemy":
                addenemies.Add(m);
                break;
            case "Loose":
                if (m.GetString(0) == player.ConnectUserId) Looser = 2; else Looser = 1;
                break;
            case "error":
                errors.Add(m.GetString(0));
                break;
            case "DisconnectMuchPlayers":
                errors.Add("Уже набрано 2 игрока, извините.");
                break;
            case "YourID":
                playerID = m.GetInt(0);
                break;
            default:
                Debug.LogError("Неизвестный тип "+m.Type);
                break;
        }
    }

    public void error(PlayerIOError error)
    {
        switch (error.ErrorCode)
        {
            case ErrorCode.RoomAlreadyExists:
                errors.Add("Такая комната уже сущевствует");
                break;
            case ErrorCode.InvalidPassword:
                errors.Add("Неправильный пароль");
                ErrorWhileLoginRegister = true;
                break;
            case ErrorCode.UnknownUser:
                errors.Add("Пользователь не зарегестрирован");
                ErrorWhileLoginRegister = true;
                break;
            case ErrorCode.InvalidAuth:
                Debug.Log("InvalidAuth, check ConnectManager.cs");
                
                break;
            case ErrorCode.InvalidRegistrationData:
                errors.Add("Логин занят");
                ErrorWhileLoginRegister = true;
                break;
            default:
                errors.Add(error.Message);
                Debug.Log(error.ErrorCode + "\tMSG:" + error.Message);
                break;
        }
    }
}
