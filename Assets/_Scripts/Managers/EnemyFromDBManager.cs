﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlayerIOClient;

public class EnemyFromDBManager : MonoBehaviour
{

    public List<GameObject> enemies = new List<GameObject>();

	// Use this for initialization
	void Start () {
        if (ConnectManager.Instance.player != null)
        {
                ConnectManager.Instance.player.BigDB.LoadRange("Enemies", "ID", null, null, null, 1000,
                    delegate(DatabaseObject[] objects)
                    {
                        GameObject temp;
                        foreach (DatabaseObject obj in objects)
                        {
                            temp = enemies.Find(enemy => enemy.GetComponent<EnemyInfo>().ID == obj.GetInt("ID"));
                            temp.GetComponent<AIPath>().speed = obj.GetInt("Speed");
                            temp.GetComponent<AIPath>().turningSpeed = obj.GetInt("TurnSpeed");
                            temp.GetComponent<EnemyInfo>().HP = obj.GetInt("HP");
                            temp.GetComponent<EnemyInfo>().dmgToPlayer = obj.GetInt("Dmg");
                            temp.GetComponent<EnemyInfo>().price = obj.GetInt("Price");
                            temp.name = obj.GetString("Name");
                        }
                    }, ConnectManager.Instance.error);
        }
	}
	
	// Update is called once per frame
	void Update () {

	}
}
