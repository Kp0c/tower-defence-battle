﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlayerIOClient;

public class SpawnManager : MonoBehaviour {

    public List<GameObject> start;
    public List<GameObject> finish;
    //public GameObject startForID2;
    //public GameObject finishFotID2;

    public GameObject HPbarPref;
    public GameObject HPBarsOwner;
    public Camera UICamera;

    private GameObject temp;
    private GameObject temp2;

	void Start () {
	}

    private static void SendEnemy(int ID)
    {
        if (ConnectManager.Instance.Money >= GlobalVars.GetEnemyPrice(ID))
        {
            ConnectManager.Instance.Money -= GlobalVars.GetEnemyPrice(ID);
            ConnectManager.Instance.connection.Send("sendEnemy", ID);
        }
        else
        {
            ConnectManager.Instance.errors.Add("У вас не хватает золота на " + GlobalVars.GetEnemyFromIndex(ID).name);
        }
    }

    public void SendGoblin()
    {
        SendEnemy(0);
    }

    public void SendSuperEnemy()
    {
        SendEnemy(2);
    }

    public void SendZombie()
    {
        SendEnemy(1);
    }

    void Update()
    {
        if (ConnectManager.Instance.addenemies.Count > 0)
        {
            foreach (Message m in ConnectManager.Instance.addenemies)
            {
                if (m.GetInt(0) != 1)
                {
                    temp = Instantiate(GlobalVars.GetEnemyFromIndex(m.GetInt(1)), new Vector3(start[0].
                    transform.position.x, 0, start[0].transform.position.z), start[0].
                    transform.rotation) as GameObject;
                    temp.GetComponent<AIPath>().target = finish[0].transform;
                }

                if (m.GetInt(0) != 2)
                {
                    temp = Instantiate(GlobalVars.GetEnemyFromIndex(m.GetInt(1)), new Vector3(start[1].
                    transform.position.x, 0, start[1].transform.position.z), start[1].
                    transform.rotation) as GameObject;
                    temp.GetComponent<AIPath>().target = finish[1].transform;
                }

                temp2 = NGUITools.AddChild(HPBarsOwner, HPbarPref);
                temp2.GetComponent<UIFollowTarget>().target = temp.transform;
                temp2.GetComponent<UIFollowTarget>().uiCamera = UICamera;
                temp.GetComponent<EnemyInfo>().HPBarSlider = temp2.GetComponentInChildren<UISlider>();
                temp.GetComponent<EnemyInfo>().ForeGround = temp2.GetComponentsInChildren<UISprite>()[1];
                temp.GetComponent<EnemyInfo>().ForPlayerWithId = m.GetInt(0);
            }
            ConnectManager.Instance.addenemies.Clear();
        }
    }
}
