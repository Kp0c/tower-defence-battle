﻿using UnityEngine;
using System.Collections;

public class PlaceScript : MonoBehaviour {

    public Material materialOn;
    public Material materialOff;
    public Texture textureOn;
    public Texture textureOff;
    public int OwnerIdOnServer = 1;
    private BuildTower buildTowerManager;

	// Use this for initialization
	void Start () {
        buildTowerManager = GameObject.Find("GameManager").GetComponent<BuildTower>();
	}
	
	// Update is called once per frame
	void Update () {
        //if (GlobalVars.MouseOverGUI)
        //{
        //    gameObject.renderer.material = materialOff;
        //    gameObject.renderer.material.mainTexture = textureOff;
        //    buildTowerManager.currentPlace = null;
        //}
	}

    void OnMouseEnter()
    {
        if (ConnectManager.Instance.connection != null)
        {
            if (ConnectManager.Instance.playerID == OwnerIdOnServer)
            {
                gameObject.renderer.material = materialOn;
                gameObject.renderer.material.mainTexture = textureOn;
                buildTowerManager.currentPlace = this.gameObject;
            }
        }
    }

    void OnMouseExit()
    {
        gameObject.renderer.material = materialOff;
        gameObject.renderer.material.mainTexture = textureOff;
        buildTowerManager.currentPlace = null;
    }
}
